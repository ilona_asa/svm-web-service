package com.uai.engine;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.restlet.data.Form;
import org.restlet.data.Parameter;
import org.restlet.data.Status;
import org.restlet.engine.header.Header;
import org.restlet.ext.json.JsonRepresentation;
import org.restlet.representation.Representation;
import org.restlet.resource.Post;
import org.restlet.resource.ServerResource;
import org.restlet.util.Series;


import com.uai.utility.ConnectDB;

public class GetJsonDataRes extends ServerResource {
	private PreparedStatement statement = null, preparedState = null;

	@Post
	public Representation acceptRepresentation(Representation entity)
			throws Exception {
		Representation retRep = null;
		Form form = new Form(entity);

		Connection con = null;
		con = ConnectDB.connectDB();
		JSONObject objResult = new JSONObject();
		JSONArray arrData = new JSONArray();
		JSONArray arrLinks = new JSONArray();
		List<String> users = new ArrayList<String>();

		String username = null;
		Integer amount = null;
		for (Parameter parameter : form) {
			if (parameter.getName().equalsIgnoreCase("username")) {
				username = parameter.getValue();
			} else if (parameter.getName().equalsIgnoreCase("amount")) {
				amount = Integer.parseInt(parameter.getValue());
			}
		}

		if (amount != null && (amount%2) == 0) {
			ResultSet userData = null, tweetData = null, linkData = null;
			try {
//				statement = con
//						.prepareStatement("SELECT * FROM user ORDER BY id ASC LIMIT 0,"
//								+ amount);
				statement = con
						.prepareStatement("SELECT distinct user.* FROM user, no_duplicate, unlabeled WHERE user.user_id=no_duplicate.user_id AND no_duplicate.id=unlabeled.id AND no_duplicate.class = 'negatif' LIMIT 0,"+ amount/2 +" union select * from user where id=2 union SELECT distinct user.* FROM user, no_duplicate, unlabeled WHERE user.user_id=no_duplicate.user_id AND no_duplicate.id=unlabeled.id AND no_duplicate.class = 'positif' ORDER BY id ASC LIMIT 0,"+ amount/2);
				userData = statement.executeQuery();
				while (userData.next()) {
					JSONObject objData = new JSONObject();
					users.add(userData.getString("user_id"));

					String userID = userData.getString("user_id");
					objData.put("name", userData.getString("name"));
					objData.put("screen_name",
							userData.getString("screen_name"));
					objData.put("followers",
							userData.getString("followers_count"));
					objData.put("verified", userData.getString("verified"));
					if (userData.getString("profile_image_url_https") == null) {
						objData.put("image_url", "");
					} else {
						objData.put("image_url",
								userData.getString("profile_image_url_https"));
					}
					if (userData.getString("description") == null) {
						objData.put("desc", "");
					} else {
						objData.put("desc", userData.getString("description"));
					}
					if (userData.getString("location") == null) {
						objData.put("location", "");
					} else {
						objData.put("location", userData.getString("location"));
					}
					if (userData.getString("name").equals("KPK")) {
						objData.put("group", 1);
					} else if (userData.getString("name").equals("Polri")) {
						objData.put("group", 2);
					} else {
						objData.put("group", 5);
					}
					objData.put("verified", userData.getString("verified"));

					String tweets = "";
					preparedState = con
							.prepareStatement("SELECT tweet_id, tweet, created, class FROM no_duplicate WHERE user_id='"
									+ userID
									+ "' AND class!='?' ORDER BY id ASC");
					tweetData = preparedState.executeQuery();
					while (tweetData.next()) {
						String tweet = tweetData.getString("tweet_id") + "<br>"
								+tweetData.getString("tweet") + "<br>"
								+ tweetData.getString("created")
								+ "<br>Classify as: "
								+ tweetData.getString("class") + "<hr>";
						tweets += tweet;
					}
					objData.put("tweet", tweets);

					arrData.put(objData);
				}

				objResult.put("nodes", (Object) arrData);

				Integer source = 0, target = 0; 
				List<String> users2 = new ArrayList<String>();
				users2 = users;
				for (String from : users) {
					
					target = 0;
					for (String to : users2) {
						
						statement = con
								.prepareStatement("SELECT count(no_duplicate.tweet_id) as value, class FROM pihak, no_duplicate WHERE pihak.tweet_id=no_duplicate.tweet_id AND asal='"
										+ from
										+ "' AND tujuan='"
										+ to
										+ "' GROUP BY class");
						linkData = statement.executeQuery();
						while (linkData.next()) {
							if(source != target){
								JSONObject objData = new JSONObject();
								objData.put("source", source);
								objData.put("target", target);
								objData.put("value", linkData.getInt("value"));
								if (linkData.getString("class").equals("positif")) {
									objData.put("topic", 3);
								} else {
									objData.put("topic", 4);
								}
								arrLinks.put(objData);
							}
						}
						target++;
					}
					source++;

				}
				objResult.put("links", (Object) arrLinks);

			} catch (Exception e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}

			@SuppressWarnings("unchecked")
			Series<Header> responseHeaders = (Series<Header>) getResponse()
					.getAttributes().get("org.restlet.http.headers");
			if (responseHeaders == null) {
				responseHeaders = new Series<Header>(Header.class);
				getRequestAttributes().put("org.restlet.http.headers",
						responseHeaders);
			}
			responseHeaders
					.add(new Header("Content-Type", "application/json;"));

			retRep = new JsonRepresentation(objResult);

			return retRep;
		} else {
			getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST,
					"amount can't be null.");
			return retRep;
		}
	}

	public Integer checkDone(List<String> daftar, String x) {
		Integer check = 0;
		for (String now : daftar) {
			if (x.equals(now)) {
				check = 1;
			}
		}
		return check;
	}
}
