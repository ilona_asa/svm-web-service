package com.uai.engine;


import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.Random;

import org.json.JSONObject;
import org.restlet.data.Form;
import org.restlet.data.Parameter;
import org.restlet.data.Status;
import org.restlet.engine.header.Header;
import org.restlet.ext.json.JsonRepresentation;
import org.restlet.representation.Representation;
import org.restlet.resource.Post;
import org.restlet.resource.ServerResource;
import org.restlet.util.Series;

import weka.classifiers.Evaluation;
import weka.classifiers.functions.SMO;
import weka.core.Instances;
import weka.core.converters.ArffLoader;

public class CrossValRes extends ServerResource{

	@Post
	public Representation acceptRepresentation(Representation entity) throws Exception {
		Representation retRep = null;
		Form form = new Form(entity);
		
//		Connection con = null;
//		con = ConnectDB.connectDB();
		JSONObject objResult = new JSONObject();


		String username = null;
		for (Parameter parameter : form) {
			if (parameter.getName().equalsIgnoreCase("username")) {
				username = parameter.getValue();
			} 
		}


		//if (username != null && username.equals("asa")) {
			try {
				Instances data = null; 
				
				ArffLoader loader = new ArffLoader();
				loader.setFile(new File("D://Kuliah/Tugas_Akhir/training set baru.arff"));
				Instances structure = loader.getStructure();
				data = loader.getDataSet();
				data.setClassIndex(structure.numAttributes()-1);
				
			  //initialize SMO svm classifier
		        SMO classifier = new SMO();	     
//		        String[] options = new String[1];
//		        options[0] = "**-C 8** -L 0.0010 -P 1.0E-12 -N 0 -V 10 -W 1 -K weka.classifiers.functions.supportVector.RBFKernel -C 250007 -G 0.01";
//		        classifier.setOptions(options);
		        classifier.buildClassifier(data);

		        
		        Evaluation eval = new Evaluation(data);
		        Random rand = new Random(1);  // using seed = 1
		        int folds = 10;
		        eval.crossValidateModel(classifier, data, folds, rand);
		        String summary = eval.toSummaryString("\nSummary\n======\n", false);
		        String detail = eval.toClassDetailsString("\nDetail\n======\n");
		        String confusion = eval.toMatrixString("\nConfussion Matrix\n======\n");

		     // save labeled data
		        BufferedWriter writer = new BufferedWriter(
		                                  new FileWriter("D://Kuliah/Tugas_Akhir/result_crossVal.txt"));
		        writer.write(summary);
		        writer.newLine();
		        writer.write(detail);
		        writer.newLine();
		        writer.write(confusion);
		        writer.newLine();
		        writer.flush();
		        writer.close();
		        File file = new File("D://Kuliah/Tugas_Akhir/result_crossVal.txt");
		        long fileSize = file.length(); 
		        if (fileSize > 0){
		        	objResult.put("status", "Cross-validation Succeed"); 
		        	objResult.put("file", file.getPath()); 
		        } else {
		        	objResult.put("status", "Cross-validation Failed"); 
		        	objResult.put("file", ""); 
		        }
		     
				} catch (Exception e2) {
					// TODO Auto-generated catch block
					e2.printStackTrace();
				}
			
			
			@SuppressWarnings("unchecked")
			Series<Header> responseHeaders = (Series<Header>) getResponse()
					.getAttributes().get("org.restlet.http.headers");
			if (responseHeaders == null) {
				responseHeaders = new Series<Header>(Header.class);
				getRequestAttributes().put("org.restlet.http.headers",
						responseHeaders);
			}
			responseHeaders.add(new Header("Content-Type",
					"application/json;"));

			retRep = new JsonRepresentation(objResult);

			return retRep;
//			} else {
//			getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST,
//					"username must be not null.");
//			return retRep;
//			}
	}
}
