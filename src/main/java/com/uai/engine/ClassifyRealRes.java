package com.uai.engine;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;

import org.json.JSONObject;
import org.restlet.data.Form;
import org.restlet.data.Parameter;
import org.restlet.data.Status;
import org.restlet.engine.header.Header;
import org.restlet.ext.json.JsonRepresentation;
import org.restlet.representation.Representation;
import org.restlet.resource.Post;
import org.restlet.resource.ServerResource;
import org.restlet.util.Series;

import com.uai.utility.ConnectDB;

import weka.classifiers.functions.SMO;
import weka.core.Instances;
import weka.core.converters.ArffLoader;
import weka.experiment.InstanceQuery;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Add;
import weka.filters.unsupervised.attribute.Remove;

public class ClassifyRealRes extends ServerResource {
	private PreparedStatement statement = null;

	@Post
	public Representation acceptRepresentation(Representation entity)
			throws Exception {
		Representation retRep = null;
		Form form = new Form(entity);

		Connection con = null;
		con = ConnectDB.connectDB();
		JSONObject objResult = new JSONObject();

		String username = null;
		Integer amount = null, start = null;
		for (Parameter parameter : form) {
			if (parameter.getName().equalsIgnoreCase("username")) {
				username = parameter.getValue();
			}if(parameter.getName().equalsIgnoreCase("amount")) {
				amount = Integer.parseInt(parameter.getValue());
			}if(parameter.getName().equalsIgnoreCase("start")) {
				start = Integer.parseInt(parameter.getValue());
			}
		}

		if (amount != null && start != null) {
			try {
				int success = 0;
				Instances data = null;

				ArffLoader loader = new ArffLoader();
				loader.setFile(new File(
						"D://Kuliah/Tugas_Akhir/training set baru.arff"));
				Instances structure = loader.getStructure();
				data = loader.getDataSet();
				data.setClassIndex(structure.numAttributes() - 1);

				// initialize SMO svm classifier
				SMO classifier = new SMO();
				classifier.buildClassifier(data);

				// load unlabeled data
				InstanceQuery query = new InstanceQuery();
				query.setDatabaseURL("jdbc:mysql://localhost:3306/asa");
				query.setUsername("root");
				query.setPassword("");
				query.setQuery("select * from unlabeled order by id limit "+start+","+amount);
				
				Instances qData = query.retrieveInstances();
				String[] options = new String[2];
				 options[0] = "-R";                                    // "range"
				 options[1] = "370,371";                                     // last attribute
				 Remove remove = new Remove();                         // new instance of filter
				 remove.setOptions(options);                           // set options
				 remove.setInputFormat(qData);                          // inform filter about dataset **AFTER** setting options
				 Instances unlabeled = Filter.useFilter(qData, remove);   // apply filter


				// set class attribute
				 Add filter = new Add();
			     filter.setAttributeIndex("370");
			     filter.setNominalLabels("positif,negatif");
			     filter.setAttributeName("class");
			     filter.setInputFormat(unlabeled);
			     unlabeled = Filter.useFilter(unlabeled, filter);
				unlabeled.setClassIndex(unlabeled.numAttributes() - 1);

				// create copy
				//Instances labeled = new Instances(unlabeled);

				// label instances
				for (int i = 0; i < unlabeled.numInstances(); i++) {
					double clsLabel = classifier.classifyInstance(unlabeled
							.instance(i));
					String label = unlabeled.instance(i).classAttribute()
							.value((int) clsLabel);
					int id = (int)qData.get(i).value(370);
					//labeled.instance(i).setClassValue(label);
//					System.out.println("predicted: " + label);
					statement = con
							.prepareStatement("UPDATE no_duplicate SET class = '"
									+ label
									+ "' WHERE  id=" + id);
					success = statement.executeUpdate();
				}
				
			
				
				if (success == 0) {
					objResult.put("message", "Failed to predict.");
					objResult.put("status", "0");
				}else{
					objResult.put("message", "Succeed to predict");
					objResult.put("status", "1");
				}

			} catch (Exception e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}

			@SuppressWarnings("unchecked")
			Series<Header> responseHeaders = (Series<Header>) getResponse()
					.getAttributes().get("org.restlet.http.headers");
			if (responseHeaders == null) {
				responseHeaders = new Series<Header>(Header.class);
				getRequestAttributes().put("org.restlet.http.headers",
						responseHeaders);
			}
			responseHeaders
					.add(new Header("Content-Type", "application/json;"));

			retRep = new JsonRepresentation(objResult);

			return retRep;
		} else {
			getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST,
					"wrong username.");
			return retRep;
		}
	}
}
