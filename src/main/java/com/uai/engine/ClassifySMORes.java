package com.uai.engine;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;

import org.json.JSONObject;
import org.restlet.data.Form;
import org.restlet.data.Parameter;
import org.restlet.data.Status;
import org.restlet.engine.header.Header;
import org.restlet.ext.json.JsonRepresentation;
import org.restlet.representation.Representation;
import org.restlet.resource.Post;
import org.restlet.resource.ServerResource;
import org.restlet.util.Series;

import weka.classifiers.functions.SMO;
import weka.core.Instances;
import weka.core.converters.ArffLoader;

public class ClassifySMORes extends ServerResource {

	@Post
	public Representation acceptRepresentation(Representation entity)
			throws Exception {
		Representation retRep = null;
		Form form = new Form(entity);

		// Connection con = null;
		// con = ConnectDB.connectDB();
		JSONObject objResult = new JSONObject();

		String username = null;
		for (Parameter parameter : form) {
			if (parameter.getName().equalsIgnoreCase("username")) {
				username = parameter.getValue();
			}
		}

		if (username != null && username.equals("asa")) {
			try {
				Instances data = null;

				// source = new
				// DataSource("D://Kuliah/Tugas_Akhir/trainfixed.arff");
				ArffLoader loader = new ArffLoader();
				loader.setFile(new File(
						"D://Kuliah/Tugas_Akhir/training set baru.arff"));
				Instances structure = loader.getStructure();
				data = loader.getDataSet();
				data.setClassIndex(structure.numAttributes() - 1);

				// initialize SMO svm classifier
				SMO classifier = new SMO();
				// String[] options = new String[1];
				// options[0] =
				// "**-C 8** -L 0.0010 -P 1.0E-12 -N 0 -V -1 -W 1 -K weka.classifiers.functions.supportVector.RBFKernel -C 250007 -G 0.01";
				// classifier.setOptions(options);
				classifier.buildClassifier(data);

				// load unlabeled data
				for (int j = 4; j < 10; j++) {
					Instances unlabeled = new Instances(new BufferedReader(
							new FileReader(
									"D://Kuliah/Tugas_Akhir/unlabeled data "
											+ j + ".arff")));

					// set class attribute
					unlabeled.setClassIndex(unlabeled.numAttributes() - 1);

					// create copy
					Instances labeled = new Instances(unlabeled);

					// label instances
					for (int i = 0; i < unlabeled.numInstances(); i++) {
						double clsLabel = classifier.classifyInstance(unlabeled
								.instance(i));
						String label = unlabeled.instance(i).classAttribute()
								.value((int) clsLabel);
						labeled.instance(i).setClassValue(label);

					}
					// save labeled data
					BufferedWriter writerTest = new BufferedWriter(
							new FileWriter("D://Kuliah/Tugas_Akhir/result" + j
									+ ".arff"));
					writerTest.write(labeled.toString());
					writerTest.newLine();
					writerTest.flush();
					writerTest.close();

					File file = new File("D://Kuliah/Tugas_Akhir/result" + j
							+ ".arff");
					long fileSize = file.length();
					if ((fileSize > 0)) {
						objResult.put("status",
								"Classify Unlabeled Data Succeed");
						objResult.put("file", file.getPath());
					} else {
						objResult.put("status",
								"Classify Unlabeled Data Failed");
						objResult.put("file", "");
					}
				}

			} catch (Exception e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}

			@SuppressWarnings("unchecked")
			Series<Header> responseHeaders = (Series<Header>) getResponse()
					.getAttributes().get("org.restlet.http.headers");
			if (responseHeaders == null) {
				responseHeaders = new Series<Header>(Header.class);
				getRequestAttributes().put("org.restlet.http.headers",
						responseHeaders);
			}
			responseHeaders
					.add(new Header("Content-Type", "application/json;"));

			retRep = new JsonRepresentation(objResult);

			return retRep;
		} else {
			getResponse().setStatus(Status.CLIENT_ERROR_BAD_REQUEST,
					"wrong username.");
			return retRep;
		}
	}

}
