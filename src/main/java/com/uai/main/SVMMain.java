package com.uai.main;

import org.restlet.Component;
import org.restlet.Server;
import org.restlet.data.Protocol;

/**
 * @author asa
 * 
 */
public class SVMMain {

	public static void main(String[] args) throws Exception {
		Component component = new Component();
		Server server = component.getServers().add(Protocol.HTTP, 8088);
//		component.getServers().add(Protocol.HTTPS, 8443);
		server.getContext().getParameters().add("useForwardedForHeader", "true");
		component.getDefaultHost().attach("/SMO",new SMOApp());
		component.getDefaultHost().attach("/data",new MakeJsonApp());
		component.start();
	}
}
