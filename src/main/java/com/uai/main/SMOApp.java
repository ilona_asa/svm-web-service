package com.uai.main;

import org.restlet.Application;
import org.restlet.Restlet;
import org.restlet.routing.Router;

import com.uai.engine.ClassifyRealRes;
import com.uai.engine.ClassifySMORes;
import com.uai.engine.CrossValRes;

/**
 * @author asa
 * 
 */

public class SMOApp extends Application {
	@Override
	public synchronized Restlet createInboundRoot() {
		Router router = new Router(getContext());
		router.attach("/train", ClassifySMORes.class);
		router.attach("/classify", ClassifyRealRes.class);
		router.attach("/crossval", CrossValRes.class);
		return router;
	}
}
