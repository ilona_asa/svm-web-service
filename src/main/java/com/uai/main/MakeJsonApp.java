package com.uai.main;

import org.restlet.Application;
import org.restlet.Restlet;
import org.restlet.routing.Router;

import com.uai.engine.GetJsonDataRes;

public class MakeJsonApp extends Application {
	@Override
	public synchronized Restlet createInboundRoot() {
		Router router = new Router(getContext());
		router.attachDefault(GetJsonDataRes.class);
		return router;
	}
}
