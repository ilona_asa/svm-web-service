package com.uai.utility;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * @author asa
 * 
 */
public class ConnectDB {

	public ConnectDB() {
		ConnectDB.loadDriver();
	}

	public static void loadDriver() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			System.out.println("Succes load driver com.mysql.jdbc.Driver");
		} catch (Exception e) {
			System.err.println("Failed");
		}
	}

	public static Connection connectDB() {
		try {
			String url = "jdbc:mysql://localhost:3306/asa?";
			String userName = "root";
			String passwd = "";
//			String userName = "asa";
//			String passwd = "asa123";
			Connection con = DriverManager.getConnection(url, userName, passwd);
//			System.out
//					.println("Success connect to DB jdbc:mysql://localhost:3366/cms_hpm");
//			System.out
//					.println("Success connect to DB jdbc:mysql://localhost:3306/cms_hpm");
			return con;

		} catch (SQLException ex) {
			return null;
		}
	}

}
